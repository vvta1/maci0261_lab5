package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

class EmployeeTest {
    private Employee employee;

    @BeforeEach
    void setUp() {
        employee = new Employee();
        employee.setCnp("1234567890876");
        employee.setFirstName("Ionel");
        employee.setLastName("Pacuraru");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2500.00);
        employee.setId(0);
        System.out.println("setUp");
    }

    @AfterEach
    void tearDown() {
        System.out.println("tearDown");
    }

    @Test
    @Order(2)
    void getId() {
        try {
            assertEquals(0, employee.getId(), "The " + employee.getId() + " does not exist");
            System.out.println("get Id is ok");
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Test
    @Order(1)
    void setId() {
        try {
            employee.setId(10);
            assertEquals(10, employee.getId(), "The " + employee.getId() + " does not exist");
            System.out.println("set Id is ok");
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 4})
    void testParameterizedSetId(int id){
        employee.setId(id);
        assertEquals(id, employee.getId(), "The id does not exist");
        System.out.println("set id is ok");
    }

    @Test
    @Order(3)
    void testConstructor(){
        Employee employee1 = new Employee();
        assertEquals("", employee1.getFirstName(), "Employee without first name");
        assertEquals("", employee1.getLastName(),"Employee without last name");
        System.out.println("Constructor ok");
    }

    @Test
    @Order(4)
    void getFirstName(){
        assertEquals("Ionel", employee.getFirstName(), "The "+employee.getFirstName()+" does not exist");
        System.out.println("get first name is ok");
    }

    @Test
    @Order(6)
    void setFirstName(){
        employee.setFirstName("Ana");
        assertEquals("Ana",employee.getFirstName(),"The first name "+employee.getFirstName()+" does not exist");
        System.out.println("set first name is ok");
    }

    @Test
    @Order(5)
    void getLastName(){
        assertEquals("Pacuraru", employee.getLastName(), "The "+employee.getLastName()+" does not exist");
        System.out.println("get last name is ok");
    }

    @Test
    @Disabled
    @Order(7)
    void setLastName(){
        employee.setLastName("Pop");
        assertEquals("Pop",employee.getLastName(),"The last name "+employee.getLastName()+" does not exist");
        System.out.println("set last name is ok");
    }

}